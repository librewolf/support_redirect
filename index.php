<?php

$old_url = $_SERVER['REQUEST_URI'];
$new_url = match ($_SERVER['REQUEST_URI']) {
  "/" => "https://librewolf.net/docs/faq",
  "/firefox-help" => "https://librewolf.net/docs/faq",
  "/enhanced-tracking-protection" => "https://librewolf.net/docs/faq/#what-is-enhanced-tracking-protection",
  "/turn-off-etp-desktop" => "https://librewolf.net/docs/faq/#what-is-enhanced-tracking-protection",
  "/drm-content" => "https://librewolf.net/docs/faq/#how-do-i-enable-drm",
  "/containers" => "https://librewolf.net/docs/faq/#why-isnt-first-party-isolate-enabled-by-default",
  "/https-only-prefs" => "https://librewolf.net/docs/faq/#does-librewolf-use-https-only-mode",
  "/lockwise-alerts" => "https://librewolf.net/docs/faq/#why-is-the-built-in-password-manager-disabled",
  "/primary-password-stored-logins" => "https://librewolf.net/docs/faq/#why-is-the-built-in-password-manager-disabled",
  "/storage-permissions" => "https://librewolf.net/docs/faq/#how-do-i-stay-logged-into-specific-websites",
  "/push" => "https://librewolf.net/docs/faq/#how-do-i-enable-push-notifications",
  "/phishing-malware" => "https://librewolf.net/docs/faq/#why-do-you-disable-google-safe-browsing",
  "/addons-help" => "https://librewolf.net/docs/addons/",
  "/session-restore" => "https://librewolf.net/docs/faq/#why-is-session-restore-not-working",
  "/firefox-protection-against-fingerprinting" => "https://librewolf.net/docs/faq/#what-are-the-most-common-downsides-of-rfp-resist-fingerprinting",
  default => "https://support.mozilla.org/1/firefox/latest/unknown/en-US" . $old_url,
};

header('Location: ' . $new_url, true, 301);
